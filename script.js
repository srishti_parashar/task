let purchasedList = '';
let additionalList = '';
let totalValueString = '';
let totalValue = 0;

const el = document.getElementById('data-input');

el.addEventListener('change', handleFileSelect, false);

function handleFileSelect(evt) {
  var files = evt.target.files;

  for (var i = 0, f; f = files[i]; i++) {

    var reader = new FileReader();
    reader.onload = function (event) {
      var contents = JSON.parse(event.target.result);
      document.querySelector('.app-container').style.display = 'block';
      filterPurchasedServices(contents);
    };

    reader.readAsText(f);
  }
  document.getElementById('data-input').value = '';
}

function filterPurchasedServices(data) {
  for (const mainService of data.data.purchased_services) {
    const mainServiceName = mainService.name;
    for (const subService of mainService.purchased_office_template.purchased_office_services)
      if (subService.service_selected === undefined || subService.service_selected === null) {
        const additionalListTemplate = generateTemplate(mainServiceName, subService);
        additionalList += additionalListTemplate;
      } else {
        const purchasedListTemplate = generateTemplate(mainServiceName, subService);
        const totalValueTemplate = generateTotalTemplate(subService);
        totalValueString += totalValueTemplate;
        purchasedList += purchasedListTemplate;
      }
  }

  document.getElementById('purchased-services').innerHTML = purchasedList;
  document.getElementById('additional-services').innerHTML = additionalList;

  document.getElementById('total-section').innerHTML = totalValueString;
  document.getElementById('total-of-price').innerHTML = totalValue;
}



function generateTemplate(mainServiceName, subService) {
  const templateString = `<div class="sub-container">
  <h4>${mainServiceName}</h4>
  <div class="sub-service-container">
    <img src="${subService.image}" alt="image" class="subservice-image" />
    <div class="sub-service-price-container">
      <div class="sub-service-price">
        <span>${subService.name}</span>
        <span>${subService.price}</span>
      </div>
      <div>${subService.description}</div>
    </div>
  </div>
</div>`;
  return templateString;
}

function generateTotalTemplate(subService) {
  totalValue = totalValue + Number(subService.price);
  const templateString = `
    <div class="total-value-container">
      <span>${subService.name}</span>
      <span>${subService.price}</span>
    </div>
  `;
  return templateString;
}
